﻿/** Additions to $(STDMODULE _traits).

Copyright: Denis Shelomovskij 2011-2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module unstd.traits;


public import std.traits;

import unstd.generictuple;
import std.typecons: tuple;


// https://github.com/D-Programming-Language/phobos/pull/776
/**
Returns the element type of an array.

For ranges, see also
$(STDREF range, ElementEncodingType).
*/
alias ArrayElementType(T : T[]) = T;

///
unittest
{
	static assert( is(ArrayElementType!(int[]) == int));
	static assert(is(ArrayElementType!(int[7][8]) == int[7]));
	static assert(is(ArrayElementType!string == immutable(char)));
}

unittest
{
	static assert( is(ArrayElementType!(long[0]) == long));
	static assert(is(ArrayElementType!(int[0][]) == int[0]));
	static assert(is(ArrayElementType!(int[][0]) == int[]));

	static assert(!is(ArrayElementType!int));
}


/**
Gets the rank (number of dimensions) of a static array type.

If $(D T) isn't a static array assumes it to be a $(I zero-dimensional)
static array with single element and returns zero.
*/
template staticArrayDims(T)
{
	static if(isStaticArray!T)
		enum staticArrayDims = 1 + staticArrayDims!(ArrayElementType!T);
	else
		enum staticArrayDims = 0;
}

///
unittest
{
	static assert(staticArrayDims!int == 0);
	static assert(staticArrayDims!(int[]) == 0);
	static assert(staticArrayDims!(int[0]) == 1);
	static assert(staticArrayDims!(int[7][8]) == 2);
	static assert(staticArrayDims!(int[0][]) == 0);
	static assert(staticArrayDims!(int[][0]) == 1);
}

unittest
{
	static assert(staticArrayDims!string == 0);
	static assert(staticArrayDims!(int[0][0]) == 2);
}


/**
Gets the element type of the innermost array in a multidimensional static array type. 
Considers $(D T) to be an $(D n)-dimensional static array type.

If $(D T) isn't a static array assumes it to be a $(I zero-dimensional)
static array with single element and returns $(D T).
*/
template MultidimStaticArrayElementType(T, size_t n = staticArrayDims!T)
{
	static assert(staticArrayDims!T >= n, "Not enough static array dimensions");
	static if(n)
		alias MultidimStaticArrayElementType = MultidimStaticArrayElementType!(ArrayElementType!T, n-1);
	else
		alias MultidimStaticArrayElementType = T;
}

///
unittest
{
	static assert(is(MultidimStaticArrayElementType!int == int));
	static assert(is(MultidimStaticArrayElementType!(int[]) == int[]));
	static assert(is(MultidimStaticArrayElementType!(int[0]) == int));
	static assert(!__traits(compiles, MultidimStaticArrayElementType!(int[7][8], 3)));
	static assert(is(MultidimStaticArrayElementType!(int[7][8]) == int));
	static assert(is(MultidimStaticArrayElementType!(int[7][8], 1) == int[7]));
	static assert(is(MultidimStaticArrayElementType!(int[7][8], 0) == int[7][8]));
	static assert(is(MultidimStaticArrayElementType!(int[0][]) == int[0][]));
	static assert(is(MultidimStaticArrayElementType!(int[][0]) == int[]));
}

unittest
{
	static assert(is(MultidimStaticArrayElementType!string == string));
}


/**
Calculates the total element count of a multidimensional static array.
Considers $(D T) to be an $(D n)-dimensional static array type.

If $(D T) isn't a static array assumes it to be a $(I zero-dimensional)
static array with single element and returns 1.
*/
template multidimStaticArrayElementCount(T, size_t n = staticArrayDims!T)
{
	static assert(staticArrayDims!T >= n, "Not enough static array dimensions");
	enum multidimStaticArrayElementCount = T.sizeof / MultidimStaticArrayElementType!(T, n).sizeof;
}

///
unittest
{
	static assert(multidimStaticArrayElementCount!int == 1);
	static assert(multidimStaticArrayElementCount!(int[]) == 1);
	static assert(multidimStaticArrayElementCount!(int[0]) == 0);
	static assert(!__traits(compiles, multidimStaticArrayElementCount!(int[7][8], 3)));
	static assert(multidimStaticArrayElementCount!(int[7][8]) == 7 * 8);
	static assert(multidimStaticArrayElementCount!(int[7][8], 1) == 8);
	static assert(multidimStaticArrayElementCount!(int[7][8], 0) == 1);
	static assert(multidimStaticArrayElementCount!(int[0][]) == 1);
	static assert(multidimStaticArrayElementCount!(int[][0]) == 0);
}

unittest
{
	static assert(multidimStaticArrayElementCount!string == 1);
}


/**
Get, as an expression tuple, multidimensional static array lengths considering
$(D T) to be $(D n)-dimensioanl static array.
*/
template multidimStaticArrayLengths(T, size_t n = staticArrayDims!T)
{
	static assert(staticArrayDims!T >= n, "Not enough static array dimensions");

	static if(n)
		alias multidimStaticArrayLengths = expressionTuple!(T.length, multidimStaticArrayLengths!(ArrayElementType!T, n-1));
	else
		alias multidimStaticArrayLengths = expressionTuple!();
}

///
unittest
{
	alias e1 = multidimStaticArrayLengths!(int[7][8]);
	static assert(e1.length == 2 && e1[0] == 8 && e1[1] == 7);

	alias e2 = multidimStaticArrayLengths!(int[7][8], 1);
	static assert(e2.length == 1 && e2[0] == 8);
	static assert(multidimStaticArrayLengths!(int[7][8], 0).length == 0);
}

unittest
{
	static assert(multidimStaticArrayLengths!int.length == 0);
	static assert(multidimStaticArrayLengths!(int[]).length == 0);
	static assert(multidimStaticArrayLengths!string.length == 0);
	static assert(multidimStaticArrayLengths!(int[0]) == expressionTuple!(0));
	static assert(!__traits(compiles, multidimStaticArrayLengths!(int[7][8], 3)));
	static assert(multidimStaticArrayLengths!(int[7][8]) == expressionTuple!(8, 7));
	static assert(multidimStaticArrayLengths!(int[7][8], 1) == expressionTuple!(8));
	static assert(multidimStaticArrayLengths!(int[7][8], 0).length == 0);
	static assert(multidimStaticArrayLengths!(int[0][]).length == 0);
	static assert(multidimStaticArrayLengths!(int[][0]) == expressionTuple!(0));
}


/// Detect whether tuple $(D A) is $(D PackedGenericTuple).
enum isPackedTuple(alias A) = __traits(compiles, A.Tuple);

/// ditto
enum isPackedTuple(A) = false;


/**
Get all types $(D T) include except $(D Extracted) without duplicates
in such order that every compound type precedes types it includes.
*/
template ExtractTypes(T, Extracted...) if(isTypeTuple!Extracted)
{
	static if(staticIndexOf!(T, Extracted) != -1)
	{
		alias ExtractTypes = TypeTuple!();
	}
	else
	{
		static assert(!is(T == typedef), "typedef-s aren't supported by ExtractTypes.");

		template Extract(U)
		{
			alias Extract = .ExtractTypes!(U, Extracted, T);
		}

		static if(is(PointerTarget!T PT))
		{
			alias ExtractTypes = TypeTuple!(T, Extract!PT);
		}
		else static if(__traits(isScalar, T))
		{
			alias ExtractTypes = TypeTuple!T;
		}
		else static if(is(T == struct) || is(T == class) || is(T == union))
		{
			alias ExtractTypes = TypeTuple!(T, NoDuplicates!(MapTuple!(Extract, FieldTypeTuple!T)));
		}
		else static if(isArray!T)
		{
			alias ExtractTypes = TypeTuple!(T, Extract!(ArrayElementType!T));
		}
		else
			static assert(0);
	}
}

///
unittest
{
	static assert(is(ExtractTypes!int == TypeTuple!int));
	static assert(is(ExtractTypes!(int*) == TypeTuple!(int*, int)));
	static assert(is(ExtractTypes!(int*, int) == TypeTuple!(int*)));

	static struct S1 { int i; real r; }
	static assert(is(ExtractTypes!S1 == TypeTuple!(S1, int, real)));
	static assert(is(ExtractTypes!(S1, int) == TypeTuple!(S1, real)));

	static struct S2
	{
		int* iptr;
		S1* s1ptr1, s1ptr2;
		S2[] s2darr;
		S2[3]* s2sarr;
	}
	static assert(is(ExtractTypes!S2 == TypeTuple!(
		S2,                // for `S2` itself
		int*, int,         // for `int*`
		S1*, S1, real,     // for `S1*`
		S2[],              // for `S2[]`
		S2[3]*, S2[3]      // for `S2[3]*`
	)));
}

unittest
{
	static assert(!__traits(compiles, ExtractTypes!(int*, 0)));

	static struct S1 { int i; real r; }

	static class C { real n; }
	static assert(is(ExtractTypes!C == TypeTuple!(C, real)));

	static struct S3 { C c; S1* s1ptr1, s1ptr2; C* cptr; }
	static assert(is(ExtractTypes!S3 == TypeTuple!(S3, C, real, S1*, S1, int, C*)));
}


/**
true iff $(D T) is a type. Usable for analysing generic tuples.
*/
enum isType(T) = true;

/// ditto
enum isType(alias T) = false;

///
unittest
{
	static assert(isType!int && isType!string);
	static assert(!isType!0 && !isType!"str");
	static assert(!isType!isType);
}

unittest
{
	static assert(isType!(int[]));
	static assert(isType!(TypeTuple!string));
	static assert(!__traits(compiles, isType!()));
	static assert(!__traits(compiles, isType!(int, string)));

	static assert(!isType!'a');

	static @property void __vp() { }
	static @property int __ip() { return 0; }
	static assert(!isType!(__vp));
	static assert(!isType!(__ip));

	static void __vf() { }
	static int __if() { return 0; }
	//static assert(!isType!(__vf())); //FIXME
	static assert(!isType!(__if()));
}
